#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"
#include "tests.h"

// void* heap;

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

static size_t count_nonfree_blocks(struct block_header* heap) {
    size_t res = 0;
    while (heap) {
        if (!heap->is_free) res++;
        heap = heap->next;
    }
    return res;
}

// Проверка выделения памяти
void test1(struct block_header* heap) {
    size_t block_size = 500;

    debug_heap(stdout, heap);

    printf("Test 1:\n");
    printf("Allocating a single block\n");

    void* block1 = _malloc(block_size);
    if(block1 == NULL) fprintf(stdout, "Allocation failed\n");

    debug_heap(stdout, heap);

    struct block_header* block_header = block_get_header(block1);

    if (block_header->is_free) fprintf(stdout, "Block should not be free\n");
    if (block_header->capacity.bytes != block_size || heap->capacity.bytes != block_size) fprintf(stdout, "Block capacity set incorrectly\n");

    _free(block1);
    printf("\nTest 1 passed\n\n");
    debug_heap(stdout, heap);
}

// Освобождение одного блока из нескольких выделенных
void test2(struct block_header* heap) {
    size_t block_size1 = 200;
    size_t block_size2 = 300;

    printf("Test 2:\n");
    printf("Freeing a single block from mutilpe blocks\n");

    void* block1 = _malloc(block_size1);
    if(block1 == NULL) fprintf(stdout, "Block 1 allocation failed\n");
    void* block2 = _malloc(block_size2);
    if(block2 == NULL) fprintf(stdout, "Block 2 allocation failed\n");
    debug_heap(stdout, heap);
    
    _free(block2);
    debug_heap(stdout, heap);

    struct block_header* block_header = block_get_header(block2);
    if (!block_header->is_free) fprintf(stdout, "Block should be free\n");
    if (count_nonfree_blocks(heap) != 1) fprintf(stdout, "Block 2 was not cleared correctly\n");

    _free(block1);
    printf("\nTest 2 passed\n\n");
    debug_heap(stdout, heap);
}

// Освобождение двух блоков из нескольких выделенных
void test3(struct block_header* heap) {
    size_t block_size1 = 200;
    size_t block_size2 = 300;
    size_t block_size3 = 400;

    printf("Test 3:\n");
    printf("Freeing two blocks from mutilpe blocks\n");

    void* block1 = _malloc(block_size1);
    if(block1 == NULL) fprintf(stdout, "Block 1 allocation failed\n");
    void* block2 = _malloc(block_size2);
    if(block2 == NULL) fprintf(stdout, "Block 2 allocation failed\n");
    void* block3 = _malloc(block_size3);
    if(block3 == NULL) fprintf(stdout, "Block 3 allocation failed\n");

    debug_heap(stdout, heap);

    _free(block3);
    _free(block2);

    debug_heap(stdout, heap);

    struct block_header* block_header3 = block_get_header(block3);
    if (!block_header3->is_free) fprintf(stdout, "Block 3 should be free\n");
    struct block_header* block_header2 = block_get_header(block2);
    if (!block_header2->is_free) fprintf(stdout, "Block 2 should be free\n");
    if (count_nonfree_blocks(heap) != 1) fprintf(stdout, "Block 2 or/and 3 were not cleared correctly\n");

    _free(block1);
    printf("\nTest 3 passed\n\n");
    debug_heap(stdout, heap);
}

// Память закончилась, новый регион памяти расширяет старый
void test4(struct block_header* heap) {
    size_t block_size1 = 20000;

    printf("Test 4:\n");
    printf("Out of memory, extending region\n");

    void* block1 = _malloc(block_size1);
    if(block1 == NULL) fprintf(stdout, "Block 1 allocation failed\n");

    debug_heap(stdout, heap);

    if (count_nonfree_blocks(heap) != 1) fprintf(stdout, "Block extending region was not allocated correctly\n");

    _free(block1);
    printf("\nTest 4 passed\n\n");
    debug_heap(stdout, heap);
}

void test5(struct block_header* heap) {
    size_t block_size1 = 10000;

	printf("Test 5:\n");
    printf("Out of memory, extending region\n");
	
	void* block1 = _malloc(block_size1);

	if (block1 == NULL) fprintf(stdout, "Block 1 allocation failed\n");
	
	struct block_header* addr = heap;
	while (addr->next != NULL) addr = addr->next;
	map_pages((uint8_t *) addr + size_from_capacity(addr->capacity).bytes, 1000, MAP_FIXED_NOREPLACE);
	
	void* block2 = _malloc(30000);
	
	if (block_get_header(block2) == addr) fprintf(stdout, "Test 5 failed (allocating near)\n");
	
	debug_heap(stdout, heap);
	
	_free(block1);
	_free(block2);

    printf("\nTest 5 passed\n\n");

    debug_heap(stdout, heap);
}
