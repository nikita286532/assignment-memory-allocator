#include <stdio.h>
#include <string.h>
#include "mem_internals.h"
#include "mem.h"
#include "tests.h"

int main() {

  struct block_header* heap = (struct block_header*) heap_init(8175);

  if (heap != NULL) {
      test1(heap); 
      test2(heap); 
      test3(heap); 
      test4(heap);
      test5(heap);

      fprintf(stdout, "All Tests passed\n");
  } else {
    fprintf(stdout, "Heap initialization failed\n");
  }

  return 0;
}
